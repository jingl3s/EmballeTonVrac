import os

# import PyPDF2
import pdfplumber

DOSSIER_CIBLE = "sorties"
work_path = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..", DOSSIER_CIBLE)
)


fichier_travail = os.path.join(work_path, "Invoice_231.pdf")


order = list()
with pdfplumber.open(fichier_travail) as pdf:
    first_page = pdf.pages[0]
    print(first_page.chars[0])
    table_content = pdf.pages[0].extract_table()
    for line_of_table in table_content:
        line_order = dict()
        line_order["quantite"] = line_of_table[0]
        line_order["commande"] = " ".join(line_of_table[1].split()[:-1])
        line_order["volume"] = line_of_table[1].split()[-1]
        line_order["prix_unitaire"] = line_of_table[2]
        line_order["prix_total"] = line_of_table[4]
        order.append(line_order)

print(order)


# with PyPDF2.PdfFileReader(fichier_travail) as file:
# print(file.numPages)

pass
