#!/usr/bin/python3
#-*-coding:utf8;-*-
'''
@author: jingl3s

'''

# license
#
# Producer 2018 jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import logging
import sys


def do_stuff(username, password):
    logging.basicConfig(level=logging.DEBUG)

    from packages import interraction_site
    cmde = interraction_site.InterractionSite(
        "https://nousrire.com/mon-compte/", p_test_mode=True)
    cmde.retrieve_cmd(username, password)


if __name__ == '__main__':
    
    if len(sys.argv) == 3:
        do_stuff(sys.argv[1], sys.argv[2])
    else:
        raise ValueError("Missing username and password")
